# Portfolio Architecture Website


## Quick-start

```bash
git clone https://gitlab.com/weimeilin/mypublicsite
cd mypublicsite
npm install && npm run start:dev
```

Or run from Docker
```bash
docker pull quay.io/weimei79/pawebsite
docker run -d -p 8080:8080 quay.io/weimei79/pawebsite
```