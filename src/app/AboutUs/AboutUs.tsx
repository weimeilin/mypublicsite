import * as React from 'react';
import { PageSection, Title } from '@patternfly/react-core';

const AboutUs: React.FunctionComponent = () => (
  <PageSection>
    <Title headingLevel="h1" size="lg">Portfolio Architecture</Title>
  </PageSection>
)

export { AboutUs };
