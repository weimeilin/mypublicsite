import * as React from 'react';
import { ExclamationTriangleIcon } from '@patternfly/react-icons';
import {
  Breadcrumb, 
  BreadcrumbItem, 
  BreadcrumbHeading,
  Page,
  PageSection,
  PageSectionVariants,
  SkipToContent,
  Title
  
} from '@patternfly/react-core';
import { css } from '@patternfly/react-styles';
import BellIcon from '@patternfly/react-icons/dist/js/icons/bell-icon';
import CogIcon from '@patternfly/react-icons/dist/js/icons/cog-icon';
import HelpIcon from '@patternfly/react-icons/dist/js/icons/help-icon';
import ModuleIcon from '@patternfly/react-icons/dist/js/icons/module-icon';

import { withRouter } from 'react-router-dom';
const qs = require('query-string');

const imgBrand = "https://www.patternfly.org/v4/v4/images/pfLogo.ffdafb0c74aa4c9c011251aa8f0c144c.svg";
const imgAvatar = "https://www.patternfly.org/v4/v4/images/avatarImg.6daf7202106fbdb9c72360d30a6ea85d.svg";
import Asciidoc from 'react-asciidoc';

var title;


class ArchitectureDetail extends React.Component {

  
  

  constructor(props) {
    super(props);
    this.state = {
      isDropdownOpen: false,
      isKebabDropdownOpen: false,
      activeItem: 0,
      data: 'TEST'
    };
    
  }

  

  componentDidMount() {
    const parsed = qs.parse(location.search);
    console.log("Access doc name -> "+parsed.docname);
    title = parsed.title;
    fetch("/redhatdemocentral/portfolio-architecture-examples/-/raw/main/"+parsed.docname,{
      headers : { 
          method: "get",
          'Accept': 'text/asciidoc'
       }
      })
        .then(response => response.text())
        .then(data => this.setState({ data: data }));
        //.then(data => console.log("data->",data));
        //.then(data => this.setState({ totalReactPackages: data.total }));
  }

  render() {
    

    //<Asciidoc>{adocurl}</Asciidoc>
   
    
    const pageId = 'main-content-page-layout-tertiary-nav';
    const PageSkipToContent = <SkipToContent href={`#${pageId}`}>Skip to content</SkipToContent>;

    

    return (
      
      <React.Fragment>
        <Page
          isManagedSidebar
          skipToContent={PageSkipToContent}
          mainContainerId={pageId}
          //additionalGroupedContent={headerContent}
          groupProps={{
            sticky: 'top'
          }}
        >
          
          <PageSection>
            <Breadcrumb> 
              <BreadcrumbItem to="#">Portfolio Architecture</BreadcrumbItem>
              <BreadcrumbItem to="#" isActive>{title}</BreadcrumbItem>
            </Breadcrumb>
          </PageSection>
          <PageSection>
            <Title headingLevel="h2">{title}</Title>
          <br/><br/>
          <Asciidoc>{this.state.data}</Asciidoc>
          
          </PageSection>
          <PageSection variant={PageSectionVariants.dark}>Footer</PageSection>
        </Page>
      </React.Fragment>
    );
  }
}
export { ArchitectureDetail};

