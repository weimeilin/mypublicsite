import * as React from 'react';
import { NavLink, useLocation, useHistory } from 'react-router-dom';
import {
  
  Page,
  PageHeader,
  PageSidebar,
  SkipToContent,
  Checkbox
} from '@patternfly/react-core';
import logo from '@app/bgimages/logo.png';
interface IAppLayout {
  children: React.ReactNode;
}

const AppLayout: React.FunctionComponent<IAppLayout> = ({ children }) => {
  const [isMobileView, setIsMobileView] = React.useState(true);
  
  const onPageResize = (props: { mobileView: boolean; windowSize: number }) => {
    setIsMobileView(props.mobileView);
  };

  function LogoImg() {
    const history = useHistory();
    function handleClick() {
      history.push('/');
    }
    return (
      <img src={logo} width="200" height="60" onClick={handleClick} alt="PA Logo" />
     
    );
  }

  const Header = (
    <PageHeader
      logo={<LogoImg />}

    />
  );

  const location = useLocation();

  const Sidebar = (
   < p /> 
  );

  const pageId = 'primary-app-container';

  const PageSkipToContent = (
    <SkipToContent onClick={(event) => {
      event.preventDefault();
      const primaryContentContainer = document.getElementById(pageId);
      primaryContentContainer && primaryContentContainer.focus();
    }} href={`#${pageId}`}>
      Skip to Content
    </SkipToContent>
  );
  return (
    <Page
      mainContainerId={pageId}
      header={Header}
      sidebar={Sidebar}
      onPageResize={onPageResize}
      skipToContent={PageSkipToContent}>
      {children}
    </Page>
  );
}

export { AppLayout };
